package it.polimi.como.ingsw16;

import java.util.HashMap;
import java.util.Map;

public class BattagliaNavale {
	private Playground user1;
	private Playground user2;
	private Map<Integer, Integer> shipNumber = new HashMap<Integer, Integer>();
	
	
	public BattagliaNavale( int size ) {
		user1 = new Playground( size );
		user2 = new Playground( size );
		shipNumber.put(2, 2);
		shipNumber.put(3, 1);
		shipNumber.put(4, 1);
		shipNumber.put(5, 1);
	}
	
	
	public static void main(String[] args) {
		
		new BattagliaNavale( 5 );
	}

}

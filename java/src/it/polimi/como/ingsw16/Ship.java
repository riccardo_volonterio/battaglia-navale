package it.polimi.como.ingsw16;

// Test
public abstract class Ship {
	protected int size = 0;
	
	public Ship( int size ) {
		this.size = size;
	}
	
	public int size() {
		return size;
	}

}

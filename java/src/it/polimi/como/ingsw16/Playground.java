package it.polimi.como.ingsw16;

import java.util.ArrayList;
import java.util.List;

public class Playground {
	protected List<List<Ship>> grid;
	protected int size;
	
	public Playground( int size ) {
		this.size = size;
		initGrid(size);
	}
	
	protected void initGrid( int size ) {
		grid = new ArrayList<List<Ship>>(size);
		for (int x = 0; x < grid.size(); x++) {
			grid.set(x, new ArrayList<Ship>(size));
			for (int y = 0; y < grid.size(); y++) {
				grid.get(x).set(y, null);
			}
		}
	}
	
	public boolean canPut( Ship s, int startX, int startY, Direction d ) {
		int x = startX;
		int y = startY;
		
		// Default
		int offsetX = 0;
		int offsetY = 0;
		
		if( d==Direction.UP ) {
			offsetY = -1;
		} else if ( d==Direction.DOWN ) {
			offsetY = 1;
		} else if ( d==Direction.LEFT ) {
			offsetX = -1;
		} else { // RIGHT
			offsetX = 1;
		}
		
		int i = 0;
		do {
			// Outbound checks
			if( x>=size ) return false;
			if( x<0) return false;
			if( y>=size ) return false;
			if( y<0) return false;
			
			// Cell occupied
			if( grid.get(x).get(y) != null ) {
				return false;
			}
			
			// Move the x & y
			x += offsetX;
			y += offsetY;
		} while( i<s.size() );
		
		return true;
	}
	
	public void put( Ship s, int x, int y, Direction d ) {
		
	}
}

package it.polimi.como.ingsw16;

public enum Direction {
	UP,
	DOWN,
	LEFT,
	RIGHT,
}
